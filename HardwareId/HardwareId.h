#ifndef _HARDWAREID_H_
#define _HARDWAREID_H_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WinIoCtl.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

	VOID DestroyDriveProperties(_In_ PSTORAGE_DEVICE_DESCRIPTOR DriveProperties);
	BOOLEAN GetPhysicalDriveProperties(_Out_ PSTORAGE_DEVICE_DESCRIPTOR* DriveProperties);

#ifdef __cplusplus
}
#endif

#endif
